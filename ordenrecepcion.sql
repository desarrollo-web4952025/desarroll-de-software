-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2023 a las 19:29:30
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ordenrecepcion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorio_adicional`
--

CREATE TABLE `accesorio_adicional` (
  `id_accesorios_adi` int(5) NOT NULL,
  `accesorios_adi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `accesorio_adicional`
--

INSERT INTO `accesorio_adicional` (`id_accesorios_adi`, `accesorios_adi`) VALUES
(1, 'Mouse'),
(2, 'Maletín'),
(3, 'Cargador'),
(4, 'Adaptador USB (WIFI/BLUETOOTH)'),
(5, 'Protector de teclado'),
(6, 'Protector de pantalla '),
(7, 'Cable de datos'),
(8, 'Ninguno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_admin` int(5) NOT NULL,
  `name_admin` varchar(40) NOT NULL,
  `apell_paterno` varchar(25) NOT NULL,
  `apell_materno` varchar(25) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `contraseña` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `name_admin`, `apell_paterno`, `apell_materno`, `usuario`, `contraseña`) VALUES
(1, 'Freddy', 'Vicente', 'Torres', 'freddyv', 'freddyv98576'),
(2, 'carlos', 'torres','avila','carlosate','carlitos1234'),
(3, 'Abraham', 'Sucasaca', 'Quito', 'abrahams', 'abrahams566');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(5) NOT NULL,
  `name_cliente` varchar(15) NOT NULL,
  `ape_cliente` varchar(20) NOT NULL,
  `telefono` int(9) NOT NULL,
  `correo` varchar(20) NOT NULL,
  `id_distrito` int(5) NOT NULL,
  `id_tipo_doc` int(5) NOT NULL,
  `num_doc` char(11) NOT NULL,
  `id_tipo_clt` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `name_cliente`, `ape_cliente`, `telefono`, `correo`, `id_distrito`, `id_tipo_doc`, `num_doc`, `id_tipo_clt`) VALUES
(1, 'Alex', 'Ayal', 972190071, 'giovani@hotmail.com', 1, 1, '87128391', 1),
(28, 'MOVISTARA', 'MOVISTARA', 972189012, 'movistar@gmail.com', 1, 2, '12345678901', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE `distrito` (
  `id_distrito` int(11) NOT NULL,
  `Distrito` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `distrito`
--

INSERT INTO `distrito` (`id_distrito`, `Distrito`) VALUES
(1, 'Lima'),
(2, 'Ancón'),
(3, 'San Martín de Porres'),
(4, 'Los Olivos'),
(5, 'Miraflores'),
(6, 'San Isidro'),
(7, 'Magdalena de Mar'),
(8, 'San Miguel'),
(9, 'Rímac'),
(10, 'Santiago de Surco'),
(11, 'Independencia'),
(12, 'Carabayllo'),
(13, 'Comas'),
(14, 'Lince'),
(15, 'Puente Piedra'),
(16, 'Surquillo'),
(17, 'Villa el Salvador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `nro_serie` varchar(15) NOT NULL,
  `id_tipo_equipo` int(5) NOT NULL,
  `id_marca` int(5) NOT NULL,
  `modelo` varchar(25) NOT NULL,
  `usa_equipo` varchar(20) NOT NULL,
  `pass_equipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`nro_serie`, `id_tipo_equipo`, `id_marca`, `modelo`, `usa_equipo`, `pass_equipo`) VALUES
('97MTR34', 2, 19, 'EPSILON', 'alex', 'alex12'),
('97MTR35', 1, 1, 'EPSILON13', 'movistar.cl', 'movistar1212'),
('97MTR52', 1, 2, 'EPSILON', 'movistar.cl', 'movistar12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(5) NOT NULL,
  `marca` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_marca`, `marca`) VALUES
(1, 'HP'),
(2, 'Lenovo'),
(3, 'Dell'),
(4, 'Acer'),
(5, 'Asus'),
(6, 'Samsung'),
(7, 'Apple'),
(8, 'Epson'),
(9, 'Canon'),
(10, 'Brother'),
(11, 'Cisco'),
(12, 'Huawei'),
(13, 'LG'),
(14, 'Tp-Link'),
(15, 'NetGear'),
(16, 'ViewSonic'),
(17, 'Antryx'),
(18, 'Teros'),
(19, 'IBM'),
(20, 'Oracle');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo_ingreso`
--

CREATE TABLE `motivo_ingreso` (
  `id_motivo_ingreso` int(5) NOT NULL,
  `motivo_ingreso` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `motivo_ingreso`
--

INSERT INTO `motivo_ingreso` (`id_motivo_ingreso`, `motivo_ingreso`) VALUES
(1, 'Evaluación'),
(2, 'Mantenimiento preventivo'),
(3, 'Reparación'),
(4, 'Cambio de componente'),
(5, 'Repotenciación'),
(6, 'Instalación de programas'),
(7, 'Formateo de disco'),
(8, 'Recuperación de datos'),
(9, 'Reparación estructural'),
(10, 'Garantía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacion_estructural`
--

CREATE TABLE `observacion_estructural` (
  `id_oe` int(5) NOT NULL,
  `observacion_estructural` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `observacion_estructural`
--

INSERT INTO `observacion_estructural` (`id_oe`, `observacion_estructural`) VALUES
(1, 'Pantalla rota'),
(2, 'placa sucia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_motivo_ingreso`
--

CREATE TABLE `orden_motivo_ingreso` (
  `id_orden_motivo` int(5) NOT NULL,
  `id_or` int(5) UNSIGNED ZEROFILL NOT NULL,
  `id_motivo_ingreso` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orden_motivo_ingreso`
--

INSERT INTO `orden_motivo_ingreso` (`id_orden_motivo`, `id_or`, `id_motivo_ingreso`) VALUES
(1, 00001, 1),
(2, 00001, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_recepcion`
--

CREATE TABLE `orden_recepcion` (
  `id_or` int(5) UNSIGNED ZEROFILL NOT NULL,
  `id_cliente` int(5) NOT NULL,
  `nro_serie` varchar(15) NOT NULL,
  `id_admin` int(5) NOT NULL,
  `id_accesorios_adi` int(5) NOT NULL,
  `fecha_ingreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `orden_recepcion`
--

INSERT INTO `orden_recepcion` (`id_or`, `id_cliente`, `nro_serie`, `id_admin`, `id_accesorios_adi`, `fecha_ingreso`) VALUES
(00001, 1, '97MTR52', 1, 1, '2023-05-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `or_observacion_estructural`
--

CREATE TABLE `or_observacion_estructural` (
  `id_orden_observacion` int(5) NOT NULL,
  `id_or` int(5) UNSIGNED ZEROFILL NOT NULL,
  `id_oe` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `or_observacion_estructural`
--

INSERT INTO `or_observacion_estructural` (`id_orden_observacion`, `id_or`, `id_oe`) VALUES
(1, 00001, 1),
(2, 00001, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `id_tipo_clt` int(2) NOT NULL,
  `tipo_clt` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_cliente`
--

INSERT INTO `tipo_cliente` (`id_tipo_clt`, `tipo_clt`) VALUES
(1, 'Natural'),
(2, 'Empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id_tipo_doc` int(2) NOT NULL,
  `tipo_documento` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id_tipo_doc`, `tipo_documento`) VALUES
(1, 'DNI'),
(2, 'RUC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_equipo`
--

CREATE TABLE `tipo_equipo` (
  `id_tipo_equipo` int(5) NOT NULL,
  `tipo_equipo` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_equipo`
--

INSERT INTO `tipo_equipo` (`id_tipo_equipo`, `tipo_equipo`) VALUES
(1, 'PC'),
(2, 'Laptop'),
(3, 'Impresora'),
(4, 'ALL IN ONE'),
(5, 'Servidor'),
(6, 'Switch'),
(7, 'Monitor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesorio_adicional`
--
ALTER TABLE `accesorio_adicional`
  ADD PRIMARY KEY (`id_accesorios_adi`);

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `id_distrito` (`id_distrito`),
  ADD KEY `id_tipo_clt` (`id_tipo_clt`),
  ADD KEY `id_tipo_doc` (`id_tipo_doc`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`id_distrito`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`nro_serie`),
  ADD KEY `id_tipo_equipo` (`id_tipo_equipo`),
  ADD KEY `id_marca` (`id_marca`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `motivo_ingreso`
--
ALTER TABLE `motivo_ingreso`
  ADD PRIMARY KEY (`id_motivo_ingreso`);

--
-- Indices de la tabla `observacion_estructural`
--
ALTER TABLE `observacion_estructural`
  ADD PRIMARY KEY (`id_oe`);

--
-- Indices de la tabla `orden_motivo_ingreso`
--
ALTER TABLE `orden_motivo_ingreso`
  ADD PRIMARY KEY (`id_orden_motivo`),
  ADD KEY `id_or` (`id_or`),
  ADD KEY `id_motivo_ingreso` (`id_motivo_ingreso`);

--
-- Indices de la tabla `orden_recepcion`
--
ALTER TABLE `orden_recepcion`
  ADD PRIMARY KEY (`id_or`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `nro_serie` (`nro_serie`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_accesorios_adi` (`id_accesorios_adi`);

--
-- Indices de la tabla `or_observacion_estructural`
--
ALTER TABLE `or_observacion_estructural`
  ADD PRIMARY KEY (`id_orden_observacion`),
  ADD KEY `id_or` (`id_or`),
  ADD KEY `id_oe` (`id_oe`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`id_tipo_clt`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id_tipo_doc`);

--
-- Indices de la tabla `tipo_equipo`
--
ALTER TABLE `tipo_equipo`
  ADD PRIMARY KEY (`id_tipo_equipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesorio_adicional`
--
ALTER TABLE `accesorio_adicional`
  MODIFY `id_accesorios_adi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `distrito`
--
ALTER TABLE `distrito`
  MODIFY `id_distrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id_marca` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `motivo_ingreso`
--
ALTER TABLE `motivo_ingreso`
  MODIFY `id_motivo_ingreso` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `observacion_estructural`
--
ALTER TABLE `observacion_estructural`
  MODIFY `id_oe` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `orden_motivo_ingreso`
--
ALTER TABLE `orden_motivo_ingreso`
  MODIFY `id_orden_motivo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `orden_recepcion`
--
ALTER TABLE `orden_recepcion`
  MODIFY `id_or` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `or_observacion_estructural`
--
ALTER TABLE `or_observacion_estructural`
  MODIFY `id_orden_observacion` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `id_tipo_clt` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id_tipo_doc` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_equipo`
--
ALTER TABLE `tipo_equipo`
  MODIFY `id_tipo_equipo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `Cliente_ibfk_1` FOREIGN KEY (`id_distrito`) REFERENCES `distrito` (`id_distrito`),
  ADD CONSTRAINT `Cliente_ibfk_2` FOREIGN KEY (`id_tipo_clt`) REFERENCES `tipo_cliente` (`id_tipo_clt`),
  ADD CONSTRAINT `Cliente_ibfk_3` FOREIGN KEY (`id_tipo_doc`) REFERENCES `tipo_documento` (`id_tipo_doc`);

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `Equipo_ibfk_1` FOREIGN KEY (`id_tipo_equipo`) REFERENCES `tipo_equipo` (`id_tipo_equipo`),
  ADD CONSTRAINT `Equipo_ibfk_2` FOREIGN KEY (`id_marca`) REFERENCES `marca` (`id_marca`);

--
-- Filtros para la tabla `orden_motivo_ingreso`
--
ALTER TABLE `orden_motivo_ingreso`
  ADD CONSTRAINT `orden_motivo_ingreso_ibfk_2` FOREIGN KEY (`id_motivo_ingreso`) REFERENCES `motivo_ingreso` (`id_motivo_ingreso`),
  ADD CONSTRAINT `orden_motivo_ingreso_ibfk_3` FOREIGN KEY (`id_or`) REFERENCES `orden_recepcion` (`id_or`);

--
-- Filtros para la tabla `orden_recepcion`
--
ALTER TABLE `orden_recepcion`
  ADD CONSTRAINT `Orden_Recepcion_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `Orden_Recepcion_ibfk_3` FOREIGN KEY (`id_admin`) REFERENCES `administrador` (`id_admin`),
  ADD CONSTRAINT `Orden_Recepcion_ibfk_4` FOREIGN KEY (`id_accesorios_adi`) REFERENCES `accesorio_adicional` (`id_accesorios_adi`);

--
-- Filtros para la tabla `or_observacion_estructural`
--
ALTER TABLE `or_observacion_estructural`
  ADD CONSTRAINT `or_observacion_estructural_ibfk_2` FOREIGN KEY (`id_oe`) REFERENCES `observacion_estructural` (`id_oe`),
  ADD CONSTRAINT `or_observacion_estructural_ibfk_3` FOREIGN KEY (`id_or`) REFERENCES `orden_recepcion` (`id_or`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
